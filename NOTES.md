TODO
----
  1. Each filesystem should have its own dedicated dispatcher.
  2. Each filesystem should have its own fuse_operations struct with the
     empty callbacks nulled out, to avoid unnecessary roundtrips to the
     runtime for not implemented callbacks. This relies on 1.
