#!/bin/sh
#|
exec "$CHICKEN_CSI" -s "$0" "$@"
|#

(import (chicken format)
        (chicken io)
        (chicken irregex)
        (chicken process)
        (chicken process-context)
        (chicken string))

(define (warn . lines)
  (parameterize ((current-output-port (current-error-port)))
    (newline)
    (for-each (lambda (s) (printf "fuse: ~a~n" s)) lines)
    (newline)))

(cond-expand
  ((or linux openbsd fuse-force-install))
  ((or freebsd macosx)
   (warn "This extension is only officially supported on Linux and OpenBSD."
         "It has been tested on this platform, but may not work correctly."
         "See https://eggs.call-cc.org/4/fuse#platform-notes for more information."))
  (else
   (warn "This extension is only officially supported on Linux and OpenBSD."
         "To force installation anyway, provide `-D fuse-force-install`.")
   (exit 78)))

(process-execute
 (get-environment-variable "CHICKEN_CSC")
 (foldl (lambda (a s)
          (if (irregex-search '(: bos "-l") s)
              `("-L" ,s . ,a)
              `("-C" ,s . ,a)))
        (command-line-arguments)
        (let ((s (with-input-from-pipe "pkg-config --libs --cflags fuse" read-line)))
          (cond
            ((string? s)
             (string-split s))
            (else
             (warn "pkg-config failed."
                   "Guessing at the correct compiler flags."
                   "This is unlikely to work.")
             (cond-expand
               (openbsd '("-lfuse" "-pthread"))
               (else    '("-lfuse" "-D_FILE_OFFSET_BITS=64" "-pthread"))))))))
