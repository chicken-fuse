;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Compile-time helpers for fuse.scm
;;;
;;; Copyright (c) 2013-2018, Evan Hanson
;;;
;;; BSD-style license. See LICENSE for details.
;;;

(import (chicken keyword))

(define-syntax begin0-let*
  (syntax-rules ()
    ((_ ((x y) . rest) . body)
     (let* ((x y) . rest) (begin . body) x))))

(define-inline (vector-for-each f v)
  (let ((l (vector-length v)))
    (do ((i 0 (fx+ i 1)))
        ((fx= i l))
      (f (vector-ref v i)))))

(define-inline (alist-cons k v l)
  (cons (cons k v) l))

(define (alist-delete k l . p)
  (let ((pred? (optional p eq?)))
    (let lp ((a '()) (l l))
      (cond ((null? l) (reverse a))
            ((pred? k (car (car l)))
             (append (reverse a) (cdr l)))
            (else (lp (cons (car l) a) (cdr l)))))))

(define (symbol->keyword x)
  (string->keyword (symbol->string x)))

(define (map-with-index f l)
  (do ((l l (cdr l))
       (i 0 (add1 i))
       (x (list) (cons (f (car l) i) x)))
      ((null? l) (reverse x))))

(define (posq o l)
  (let lp ((i 0) (l l))
    (cond ((null? l) #f)
          ((eq? o (car l)) i)
          (else (lp (+ i 1) (cdr l))))))

(define fuse-operations
  '(getattr:
    readdir:
    open:
    read:
    write:
    release:
    access:
    create:
    unlink:
    truncate:
    readlink:
    symlink:
    mknod:
    mkdir:
    rmdir:
    rename:
    link:
    chmod:
    chown:
    utimens:
    statfs:
    ioctl:
    fsync:
    flush:
    init:
    destroy:
    ;; Probably TODO.
    ;poll:
    ;flushdir:
    ;fsyncdir:
    ;opendir:
    ;releasedir:
    ;lock:
    ;flock:
    ;; Probably not TODO.
    ;fgetattr:
    ;ftruncate:
    ;getxattr:
    ;listxattr:
    ;removexattr:
    ;setxattr:
    ;read_buf:
    ;write_buf:
    ;bmap:
    ;getdir: ; Deprecated.
    ;utime: ; Deprecated.
    ))
