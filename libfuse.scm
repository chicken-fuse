;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; libfuse bindings for CHICKEN Scheme.
;;;
;;; The stacking and module APIs are left as exercises for the reader.
;;;
;;; See fuse.scm for a higher-level interface.
;;;
;;; Copyright (c) 2013-2018, Evan Hanson
;;; BSD-style license. See LICENSE for details.
;;;
;;; FUSE is Copyright (C) 2001-2007, Miklos Szeredi <miklos@szeredi.hu>
;;; under the terms of the GNU LGPLv2.
;;;

(declare
  (module (fuse libfuse))
  (export fuse_context_private_data fuse_exit fuse_exited
          fuse_new fuse_mount fuse_unmount fuse_destroy
          fuse_file_info_fh fuse_file_info_fh_set!
          fuse_file_info_flags fuse_operations_access_set!
          fuse_operations_chmod_set! fuse_operations_chown_set!
          fuse_operations_create_set! fuse_operations_destroy_set!
          fuse_operations_flush_set! fuse_operations_fsync_set!
          fuse_operations_getattr_set! fuse_operations_init_set!
          fuse_operations_link_set! fuse_operations_mkdir_set!
          fuse_operations_mknod_set! fuse_operations_open_set!
          fuse_operations_read_set! fuse_operations_readdir_set!
          fuse_operations_readlink_set! fuse_operations_release_set!
          fuse_operations_rename_set! fuse_operations_rmdir_set!
          fuse_operations_statfs_set! fuse_operations_symlink_set!
          fuse_operations_truncate_set! fuse_operations_unlink_set!
          fuse_operations_utimens_set! fuse_operations_write_set!))

(import (scheme)
        (chicken base)
        (chicken foreign))

(import-syntax (foreigners))

#>

#define FUSE_USE_VERSION 26
#include <fuse.h>

<#

#+(not openbsd)
(export fuse_operations_ioctl_set!)

#+(not openbsd)
(foreign-declare "#include <fuse/fuse_lowlevel.h>")

(define-foreign-type off_t "off_t")
(define-foreign-type dev_t "dev_t")
(define-foreign-type uid_t "uid_t")
(define-foreign-type gid_t "gid_t")
(define-foreign-type pid_t "pid_t")
(define-foreign-type mode_t "mode_t")
(define-foreign-type fuse (c-pointer (struct "fuse")))
(define-foreign-type fuse_args (c-pointer (struct "fuse_args")))
;(define-foreign-type fuse_buf (c-pointer (struct "fuse_buf")))
;(define-foreign-type fuse_bufvec (c-pointer (struct "fuse_bufvec")))
(define-foreign-type fuse_chan (c-pointer (struct "fuse_chan")))
;(define-foreign-type fuse_chan_ops (c-pointer (struct "fuse_chan_ops")))
(define-foreign-type fuse_conn_info (c-pointer (struct "fuse_conn_info")))
(define-foreign-type fuse_session (c-pointer (struct "fuse_session")))
;(define-foreign-type fuse_pollhandle (c-pointer (struct "fuse_pollhandle")))
(define-foreign-type fuse_fill_dir_t (function int (c-pointer (const c-string) (c-pointer (const (struct "stat"))) off_t)))

;(define-foreign-record-type (fuse_args "struct fuse_args")
;  (int argc fuse_args_argc fuse_args_argc_set!)
;  ((c-pointer c-string) argv fuse_args_argv fuse_args_argv_set!)
;  (bool allocated fuse_args_allocated fuse_args_allocated_set!))

(define-foreign-record-type (fuse_context "struct fuse_context")
  (fuse fuse fuse_context_fuse)
  (int uid fuse_context_uid) ; uid_t
  (int gid fuse_context_gid) ; gid_t
  (int pid fuse_context_pid) ; pid_t
  (c-pointer private_data fuse_context_private_data)
  (int umask fuse_context_umask)) ; mode_t

(define-foreign-record-type (fuse_file_info "struct fuse_file_info")
  (int flags fuse_file_info_flags)
  (unsigned-long fh_old fuse_file_info_fh_old)
  (int writepage fuse_file_info_writepage)
  (unsigned-int32 direct_io fuse_file_info_direct_io)
  (scheme-object fh fuse_file_info_fh fuse_file_info_fh_set!) ; uint64_t
  (unsigned-integer64 lock_owner fuse_file_info_lock_owner))

;(define-foreign-record-type (fuse_conn_info "struct fuse_conn_info")
;  (unsigned-int proto_major fuse_conn_info_proto_major)
;  (unsigned-int proto_minor fuse_conn_info_proto_minor)
;  (unsigned-int async_read fuse_conn_info_async_read)
;  (unsigned-int max_write fuse_conn_info_max_write)
;  (unsigned-int max_readahead fuse_conn_info_max_readahead)
;  (unsigned-int capable fuse_conn_info_capable)
;  (unsigned-int want fuse_conn_info_want)
;  (unsigned-int max_background fuse_conn_info_max_background)
;  (unsigned-int congestion_threshold fuse_conn_info_congestion_threshold)
;  (unsigned-int (reserved 23) fuse_conn_info_reserved))

(define-foreign-record-type (fuse_operations "struct fuse_operations")
  ((function int ((const c-string) (c-pointer (struct "stat"))))
   getattr
   fuse_operations_getattr
   fuse_operations_getattr_set!)
  ((function int ((const c-string) c-string size_t))
   readlink
   fuse_operations_readlink
   fuse_operations_readlink_set!)
  ((function int ((const c-string) "fuse_dirh_t" "fuse_dirfil_t"))
   getdir
   fuse_operations_getdir
   fuse_operations_getdir_set!)
  ((function int ((const c-string) mode_t dev_t))
   mknod
   fuse_operations_mknod
   fuse_operations_mknod_set!)
  ((function int ((const c-string) mode_t))
   mkdir
   fuse_operations_mkdir
   fuse_operations_mkdir_set!)
  ((function int ((const c-string)))
   unlink
   fuse_operations_unlink
   fuse_operations_unlink_set!)
  ((function int ((const c-string)))
   rmdir
   fuse_operations_rmdir
   fuse_operations_rmdir_set!)
  ((function int ((const c-string) (const c-string)))
   symlink
   fuse_operations_symlink
   fuse_operations_symlink_set!)
  ((function int ((const c-string) (const c-string)))
   rename
   fuse_operations_rename
   fuse_operations_rename_set!)
  ((function int ((const c-string) (const c-string)))
   link
   fuse_operations_link
   fuse_operations_link_set!)
  ((function int ((const c-string) mode_t))
   chmod
   fuse_operations_chmod
   fuse_operations_chmod_set!)
  ((function int ((const c-string) uid_t gid_t))
   chown
   fuse_operations_chown
   fuse_operations_chown_set!)
  ((function int ((const c-string) off_t))
   truncate
   fuse_operations_truncate
   fuse_operations_truncate_set!)
  ((function int ((const c-string) (c-pointer (struct "utimbuf"))))
   utime
   fuse_operations_utime
   #;fuse_operations_utime_set!)
  ((function int ((const c-string) fuse_file_info))
   open
   fuse_operations_open
   fuse_operations_open_set!)
  ((function int ((const c-string) c-string size_t off_t fuse_file_info))
   read
   fuse_operations_read
   fuse_operations_read_set!)
  ((function int ((const c-string) (const c-string) size_t off_t fuse_file_info))
   write
   fuse_operations_write
   fuse_operations_write_set!)
  ((function int ((const c-string) (c-pointer (struct "statvfs"))))
   statfs
   fuse_operations_statfs
   fuse_operations_statfs_set!)
  ((function int ((const c-string) fuse_file_info))
   flush
   fuse_operations_flush
   fuse_operations_flush_set!)
  ((function int ((const c-string) fuse_file_info))
   release
   fuse_operations_release
   fuse_operations_release_set!)
  ((function int ((const c-string) int fuse_file_info))
   fsync
   fuse_operations_fsync
   fuse_operations_fsync_set!)
  ((function int ((const c-string) (const c-string) (const c-string) size_t int))
   setxattr
   fuse_operations_setxattr
   #;fuse_operations_setxattr_set!)
  ((function int ((const c-string) (const c-string) c-string size_t))
   getxattr
   fuse_operations_getxattr
   #;fuse_operations_getxattr_set!)
  ((function int ((const c-string) c-string size_t))
   listxattr
   fuse_operations_listxattr
   #;fuse_operations_listxattr_set!)
  ((function int ((const c-string) (const c-string)))
   removexattr
   fuse_operations_removexattr
   #;fuse_operations_removexattr_set!)
  ((function int ((const c-string) fuse_file_info))
   opendir
   fuse_operations_opendir
   #;fuse_operations_opendir_set!)
  ((function int ((const c-string) c-pointer fuse_fill_dir_t off_t fuse_file_info))
   readdir
   fuse_operations_readdir
   fuse_operations_readdir_set!)
  ((function int ((const c-string) fuse_file_info))
   releasedir
   fuse_operations_releasedir
   #;fuse_operations_releasedir_set!)
  ((function int ((const c-string) int fuse_file_info))
   fsyncdir
   fuse_operations_fsyncdir
   #;fuse_operations_fsyncdir_set!)
  ((function c-pointer (fuse_conn_info))
   init
   fuse_operations_init
   fuse_operations_init_set!)
  ((function void ())
   destroy
   fuse_operations_destroy
   fuse_operations_destroy_set!)
  ((function int ((const c-string) int))
   access
   fuse_operations_access
   fuse_operations_access_set!)
  ((function int ((const c-string) mode_t fuse_file_info))
   create
   fuse_operations_create
   fuse_operations_create_set!)
  ;((function int ((const c-string) off_t fuse_file_info))
  ; ftruncate
  ; fuse_operations_ftruncate
  ; #;fuse_operations_ftruncate_set!)
  ;((function int ((const c-string) (c-pointer (struct "stat")) fuse_file_info))
  ; fgetattr
  ; fuse_operations_fgetattr
  ; #;fuse_operations_fgetattr_set!)
  ;((function int ((const c-string) fuse_file_info int (c-pointer (struct "flock"))))
  ; lock
  ; fuse_operations_lock
  ; #;fuse_operations_lock_set!)
  ((function int ((const c-string) (const (struct "timespec[2]"))))
   utimens
   fuse_operations_utimens
   fuse_operations_utimens_set!))
  ;((function int ((const c-string) size_t (c-pointer unsigned-integer64)))
  ; bmap
  ; fuse_operations_bmap
  ; #;fuse_operations_bmap_set!)
  ;(unsigned-int32 flag_nullpath_ok fuse_operations_flags fuse_operations_flags_set!)
  ;; NOTE ioctl is bound manually below.
  ;((function int ((const c-string) int c-pointer fuse_file_info unsigned-int c-pointer))
  ; ioctl
  ; fuse_operations_ioctl
  ; fuse_operations_ioctl_set!))
  ;((function int ((const c-string) fuse_file_info fuse_pollhandle (c-pointer unsigned-int)))
  ; poll
  ; fuse_operations_poll
  ; #;fuse_operations_poll_set!)
  ;((function int ((const c-string) fuse_bufvec off_t fuse_file_info))
  ; write_buf
  ; fuse_operations_write_buf
  ; #;fuse_operations_write_buf_set!)
  ;((function int ((const c-string) (c-pointer fuse_bufvec) size_t off_t fuse_file_info))
  ; read_buf
  ; fuse_operations_read_buf
  ; #;fuse_operations_read_buf_set!)
  ;((function int ((const c-string) fuse_file_info int))
  ; flock
  ; fuse_operations_flock
  ; #;fuse_operations_flock_set!))

(cond-expand
  (openbsd) ; No ioctl on OpenBSD.
  (else
   (define-foreign-type fuse-operations-ioctl
     (function int ((const c-string) int c-pointer fuse_file_info unsigned-int c-pointer)))
   (define fuse_operations_ioctl
     (foreign-lambda* fuse-operations-ioctl ((fuse_operations ops)) "C_return(ops->ioctl);"))
   (define fuse_operations_ioctl_set!
     (foreign-lambda* void ((fuse_operations ops) (fuse-operations-ioctl fun)) "ops->ioctl = fun;"))))

(define fuse_new (foreign-lambda fuse fuse_new fuse_chan fuse_args (const fuse_operations) size_t c-pointer))
(define fuse_destroy (foreign-lambda void fuse_destroy fuse))
(define fuse_loop (foreign-lambda int fuse_loop fuse))
(define fuse_get_context (foreign-lambda fuse_context fuse_get_context))
(define fuse_get_session (foreign-lambda fuse_session fuse_get_session fuse))
;(define fuse_getgroups (foreign-lambda int fuse_getgroups int (c-pointer gid_t)))
;(define fuse_interrupted (foreign-lambda bool fuse_interrupted))
;(define fuse_notify_poll (foreign-lambda int fuse_notify_poll fuse_pollhandle))

(cond-expand
  ((not openbsd)
   (define fuse_exit (foreign-lambda void fuse_exit fuse))
   (define fuse_exited (foreign-lambda bool fuse_exited fuse)))
  (else
   (define fuse_exit void)
   (define fuse_exited (lambda (_) #f))))

(define fuse_mount (foreign-lambda fuse_chan fuse_mount nonnull-c-string fuse_args))
(define fuse_unmount (foreign-lambda void fuse_unmount nonnull-c-string fuse_chan))

(define fuse_set_signal_handlers (foreign-lambda int fuse_set_signal_handlers fuse_session))
(define fuse_remove_signal_handlers (foreign-lambda void fuse_remove_signal_handlers fuse_session))

;(define fuse_parse_cmdline (foreign-lambda int fuse_parse_cmdline fuse_args (c-pointer c-string) (c-pointer int) (c-pointer int)))
;(define fuse_daemonize (foreign-lambda int fuse_daemonize int))
;(define fuse_version (foreign-lambda int fuse_version))
;(define fuse_pollhandle_destroy (foreign-lambda void fuse_pollhandle_destroy fuse_pollhandle))

(define fuse_loop_mt (foreign-lambda int fuse_loop_mt fuse))
;(define fuse_start_cleanup_thread (foreign-lambda int fuse_start_cleanup_thread fuse))
;(define fuse_start_cleanup_thread (foreign-lambda void fuse_stop_cleanup_thread fuse))
;(define fuse_clean_cache (foreign-lambda int fuse_clean_cache fuse))

;(define-foreign-enum-type (fuse_buf_flags unsigned-int)
;  (is-fd FUSE_BUF_IS_FD)
;  (fd-seek FUSE_BUF_FD_SEEK)
;  (fd-seek FUSE_BUF_FD_SEEK)
;  (fd-retry FUSE_BUF_FD_RETRY))

;(define-foreign-enum-type (fuse_buf_copy_flags unsigned-int)
;  (no-splice FUSE_BUF_NO_SPLICE)
;  (force-splice FUSE_BUF_FORCE_SPLICE)
;  (splice-move FUSE_BUF_SPLICE_MOVE)
;  (splice-nonblock FUSE_BUF_SPLICE_NONBLOCK))

;(define-foreign-record-type (fuse_buf "struct fuse_buf")
;  (size_t size fuse_buf_size)
;  (fuse_buf_flags flags fuse_buf_flags)
;  (c-pointer mem fuse_buf_mem)
;  (int fd fuse_buf_fd)
;  (int pos fuse_buf_pos)) ; off_t

;(define fuse_buf_size (foreign-lambda size_t fuse_buf_size (const fuse_bufvec)))
;(define fuse_buf_copy (foreign-lambda size_t fuse_buf_copy fuse_bufvec fuse_bufvec fuse_buf_copy_flags))

;(define fuse_chan_new (foreign-lambda fuse_chan fuse_chan_new fuse_chan_ops int size_t c-pointer))
(define fuse_chan_fd (foreign-lambda int fuse_chan_fd fuse_chan))
;(define fuse_chan_bufsize (foreign-lambda size_t fuse_chan_bufsize fuse_chan))
;(define fuse_chan_session (foreign-lambda fuse_session fuse_chan_session fuse_chan))
;(define fuse_chan_recv (foreign-lambda int fuse_chan_recv (c-pointer fuse_chan) c-pointer size_t))
;(define fuse_chan_send (foreign-lambda int fuse_chan_send fuse_chan (c-pointer (struct "iovec")) size_t))
;(define fuse_chan_destroy (foreign-lambda void fuse_chan_destroy fuse_chan))

;(define-foreign-record-type (fuse_session_ops "struct fuse_session_ops")
;  ((function void (c-pointer (const c-string) size_t fuse_chan))
;   process
;   fuse_session_ops_process
;   fuse_session_ops_process_set!)
;  ((function void (c-pointer int))
;   exit
;   fuse_session_ops_exit
;   fuse_session_ops_exit_set!)
;  ((function bool (c-pointer))
;   exited
;   fuse_session_ops_exited
;   fuse_session_ops_exited_set!)
;  ((function void (c-pointer))
;   destroy
;   fuse_session_ops_destroy
;   fuse_session_ops_destroy_set!))

;(define fuse_session_new (foreign-lambda fuse_session fuse_session_new fuse_session_ops c-pointer))
;(define fuse_session_add_chan (foreign-lambda void fuse_session_add_chan fuse_session fuse_chan))
;(define fuse_session_remove_chan (foreign-lambda void fuse_session_remove_chan fuse_chan))
;(define fuse_session_next_chan (foreign-lambda fuse_chan fuse_session_next_chan fuse_session fuse_chan))
;(define fuse_session_process (foreign-lambda void fuse_session_process fuse_session c-pointer size_t fuse_chan))
;(define fuse_session_process_buf (foreign-lambda void fuse_session_process_buf fuse_session (const fuse_buf) fuse_chan))
;(define fuse_session_receive_buf (foreign-lambda int fuse_session_receive_buf fuse_session fuse_buf (c-pointer fuse_chan)))
;(define fuse_session_destroy (foreign-lambda void fuse_session_destroy fuse_session))
;(define fuse_session_exit (foreign-lambda void fuse_session_exit fuse_session))
;(define fuse_session_reset (foreign-lambda void fuse_session_reset fuse_session))
;(define fuse_session_exited (foreign-lambda bool fuse_session_exited fuse_session))
;(define fuse_session_data (foreign-lambda c-pointer fuse_session_data fuse_session))
;(define fuse_session_loop (foreign-lambda int fuse_session_loop fuse_session))

;; fuse_main is defined as a macro.
(define (fuse_main argc argv op user_data)
  ((foreign-lambda* int ((int argc)
                         ((c-pointer c-string) argv)
                         (fuse_operations op)
                         (c-pointer user_data))
    "C_return(fuse_main(argc, argv, op, user_data));")
   argc
   argv
   op
   user_data))
