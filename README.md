chicken-fuse
============
A FUSE interface for CHICKEN Scheme.

Installation
------------
Installation requires the libfuse library and headers (API version 26)
and CHICKEN version 5.0 or newer.

    $ git clone http://git.foldling.org/chicken-fuse.git
    $ cd chicken-fuse
    $ chicken-install -test

Documentation
-------------
Documentation is available on the CHICKEN [wiki][], at [chickadee][],
and under the `doc/` directory.

Usage examples can be found in the `tests/` and `examples/` directories.

[wiki]: http://wiki.call-cc.org/egg/fuse
[chickadee]: http://api.call-cc.org/doc/fuse

Author
------
Evan Hanson <evhan@foldling.org>

Lots of thanks to Jörg Wittenberger and Ivan Raikov.

License
-------
3-Clause BSD. See LICENSE for details.
