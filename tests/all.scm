(import (test))

(test-group (cond-expand
              (compiling "fuse (compiled)")
              (else "fuse (interpreted)"))
  (test-group "the filesystem record type" (include-relative "fuse.scm"))
  (test-group "an empty filesystem" (include-relative "empty.scm"))
  (test-group "a hello world filesystem" (include-relative "hello.scm"))
  (test-group "directory manipulation" (include-relative "dirs.scm"))
  (test-group "exception handling" (include-relative "exn.scm")))

(test-exit)
