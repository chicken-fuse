(import (chicken bitwise)
        (chicken condition)
        (chicken format)
        (chicken load)
        (chicken pathname)
        (chicken process)
        (chicken process-context)
        (test))

(cond-expand (linux) (else (exit 0)))

(test-group "fuse (smoke test)"
  (load-relative "smoke.scm")
  (unless (test-smoke "path")
    (fprintf (current-error-port)
             "    simple mount failed\n")
    (fprintf (current-error-port)
             "    skipping other tests\n")
    (exit 1)))

(define-syntax test-interpreted
  (syntax-rules ()
    ((_ file)
     (fluid-let ((test-exit void))
       (load-relative file)))))

(define-syntax test-compiled
  (syntax-rules ()
    ((_ file)
     (let ((csc (format "csc '~a' -o compiled "
                        (pathname-replace-directory
                         (qs file)
                         (pathname-directory (program-name))))))
       (handle-exceptions _ (exit 1)
         (system* csc)
         (system* "./compiled"))))))

(test-interpreted "all.scm")
(test-compiled "all.scm")
