;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; errorfs.scm - FUSE exception handling example.
;;;
;;; Serves a filesystem that exports one file ("error") and signals an
;;; exception on any `open(2)` (pretty-printing the condition to
;;; standard error).
;;;
;;;     $ csc errorfs.scm
;;;     $ ./errorfs <mountpoint>
;;;

(import (chicken bitwise)
        (chicken condition)
        (chicken file posix)
        (chicken pretty-print)
        (chicken process signal)
        (chicken process-context)
        (chicken process-context posix)
        (chicken time)
        (fuse))

(define perm/irwxusr
  (bitwise-ior perm/irusr perm/iwusr perm/ixusr))

(define fs
  (make-filesystem
   getattr: (lambda (path)
              (or (and (string=? path "/")
                       (vector (bitwise-ior file/dir perm/irwxusr)
                               2
                               (current-user-id)
                               (current-group-id)
                               0
                               (current-seconds)
                               (current-seconds)
                               (current-seconds)))
                  (and (string=? path "/error")
                       (vector (bitwise-ior file/reg perm/irwxusr)
                               1
                               (current-user-id)
                               (current-group-id)
                               0
                               (current-seconds)
                               (current-seconds)
                               (current-seconds)))))
   readdir: (lambda (path)
              (and (string=? path "/") '("." ".." "error")))
   open:    (lambda (path mode)
              (error 'open "Error opening path" path mode))))

(for-each
 (lambda (path)
   (with-exception-handler
    (lambda (e)
      (pretty-print
       (condition->list e)
       (current-error-port)))
    (lambda ()
      (set-signal-handler! signal/int (lambda (_) (filesystem-stop! path fs)))
      (filesystem-start! path fs)
      (filesystem-wait! path fs))))
 (command-line-arguments))
